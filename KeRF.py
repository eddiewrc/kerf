#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  KeRF.py
#  
#  Copyright 2017 Daniele Raimondi <daniele.raimondi@vub.be>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import numpy as np
import sklearn
from sklearn.svm import SVC
from sklearn.datasets import load_breast_cancer
from sklearn.ensemble import RandomForestClassifier, RandomForestRegressor
from sklearn.tree import DecisionTreeRegressor, DecisionTreeClassifier, _tree
from distutils.version import LooseVersion
import matplotlib.pyplot as plt
if LooseVersion(sklearn.__version__) < LooseVersion("0.17"):
    raise Exception("treeinterpreter requires scikit-learn 0.17 or later")


def getForestPaths(model, X, verbose = False):
	ind, ptr =  model.decision_path(X)
	print ind.shape
	forestPaths = {}
	i = 0
	while i < len(X):
		#print "sample ", i
		j = 0
		forestPaths[i] = []
		while j < len(ptr)-1:
			if verbose:
				print "tree ", j
				#print ind[i,ptr[j]:ptr[j+1]]
				print ind[i,ptr[j]:ptr[j+1]].indices
			forestPaths[i].append(list(ind[i,ptr[j]:ptr[j+1]].indices))		
			j+=1			
		leaves = model.apply([X[i]])[0]	
		if verbose:	
			print "exp leaves: ",leaves
		l = 0
		while l < len(leaves):
			#print leaves[l], pathSamples[i][l][-1]
			assert leaves[l] == forestPaths[i][l][-1]
			l+=1
		i+=1
	return forestPaths # {sampleNum:[pathTree1, pathTree2, ..., []]}	
		
def KeRF_matrix(model, X, X1 = [], full=True, verbose = False):
	if len(X1) == 0:	#square matrix
		if verbose:
			print "Reading paths..."		
			forestPaths = getForestPaths(model, X, verbose = False)# {sampleNum:[pathTree1, pathTree2, ..., []]}
		if verbose:
			print "Done."	
		assert len(forestPaths) == X.shape[0]
		K = np.zeros((len(forestPaths),len(forestPaths)))
		if verbose:
			print "Populating the kernel matrix..."
		i = 0
		j = 0	
		while i < len(forestPaths):
			j = 0
			while j <= i:
				v = mKeRF(forestPaths[i], forestPaths[j], model)
				K[i][j] = v
				K[j][i] = v
				j+=1
			i+=1
		if verbose:
			print "Done."	
		return K
	else:
		if verbose:
			print "Reading paths..."		
			forestPaths = getForestPaths(model, X, verbose = False)# {sampleNum:[pathTree1, pathTree2, ..., []]}
			forestPaths1 = getForestPaths(model, X1, verbose = False)# {sampleNum:[pathTree1, pathTree2, ..., []]}
		if verbose:
			print "Done."		
		K = np.zeros((len(forestPaths),len(forestPaths1)))
		if verbose:
			print "Populating the kernel matrix..."
		i = 0
		j = 0	
		while i < len(forestPaths):
			j = 0
			while j < len(forestPaths1):
				v = mKeRF(forestPaths[i], forestPaths1[j], model)
				K[i][j] = v				
				j+=1
			i+=1
		if verbose:
			print "Done."	
		return K
	
def mKeRF(s1, s2, model): #avoid computing paths twice
	#meanSimilarity = []
	totSimilarity = [0,0]#shared, totPartitions	
	assert len(s1) == len(s2)
	i = 0
	while i < len(s1):
		#print s1
		ss1 = set(s1[i])
		ss2 = set(s2[i])
		#meanSimilarity.append(float(len(ss1.intersection(ss2)))/float(len(ss1.union(ss2))))
		totSimilarity[0] += len(ss1.intersection(ss2))
		totSimilarity[1] += len(ss1.union(ss2))
		i+=1	
	#print np.mean(meanSimilarity), float(totSimilarity[0])/float(totSimilarity[1])
	return float(totSimilarity[0])/float(totSimilarity[1])
	
def KeRF(s1, s2, mode):
	forestPaths = getForestPaths(model, [s1, s2])
	s1 = forestPaths[0]
	s2 = forestPaths[1]
	meanSimilarity = []
	totSimilarity = [0,0]#shared, totPartitions
	assert len(s1) == len(s2)
	i = 0
	while i < len(s1):
		#print s1
		ss1 = set(s1[i])
		ss2 = set(s2[i])
		meanSimilarity.append(float(len(ss1.intersection(ss2)))/float(len(ss1.union(ss2))))
		totSimilarity[0] += len(ss1.intersection(ss2))
		totSimilarity[1] += len(ss1.union(ss2))
		i+=1	
	print np.mean(meanSimilarity), float(totSimilarity[0])/float(totSimilarity[1])
	return float(totSimilarity[0])/float(totSimilarity[1])

def main():	
	data = load_breast_cancer()
	print len(data)
	SIZE = 350
	X = data["data"][:SIZE]
	Y = data["target"][:SIZE]
	x = data["data"][SIZE:]
	y = data["target"][SIZE:]
	model = RandomForestClassifier(n_estimators=10,max_depth=5, random_state=None)
	model.fit(X, Y)
	yp = model.predict_proba(x)[:,1]
	getScoresSVR(yp, y, threshold=None, invert = False, PRINT = True, CURVES = False, SAVEFIG=None)
	
	model1 = SVC(C=1.0, kernel="rbf", degree=3, gamma="auto",coef0=0.0,shrinking=True,probability=False,cache_size=2000)
	model1.fit(X, Y)
	yp1 = model1.decision_function(x)	
	getScoresSVR(yp1, y, threshold=None, invert = False, PRINT = True, CURVES = False, SAVEFIG=None)
	
	trainKern = KeRF_matrix(model, X, full=True, verbose = True)
	model2 = SVC(C=1.0, kernel="precomputed", degree=3, gamma="auto",coef0=0.0,shrinking=True,probability=False,cache_size=2000)
	model2.fit(trainKern, Y)
	print "trainkern shape ",trainKern.shape
	
	testKern = KeRF_matrix(model, x, X, full=True, verbose = True)
	print "testkern shape ",testKern.shape
	yp2 = model2.decision_function(testKern)	
	getScoresSVR(yp2, y, threshold=None, invert = False, PRINT = True, CURVES = False, SAVEFIG=None)
	
def main1():
	data = load_breast_cancer()
	X = data["data"]
	Y = data["target"]
	model = RandomForestClassifier(n_estimators=200,max_depth=20, random_state=None)
	model.fit(X, Y)
	print model
	
	'''tot = []
	for i in range(0,100):
		
		tot.append(KeRF(X[0], X[1], model))
	
	plt.violinplot(tot)
	plt.show()'''
	
	#print X[:10]
	k = KeRF_matrix(model, X[:10],)
	plt.matshow(k)
	plt.show()
	
def getScoresSVR(pred, real, threshold=None, invert = False, PRINT = False, CURVES = False, SAVEFIG=None):
	import math
	if len(pred) != len(real):
		raise Exception("ERROR: input vectors have differente len!")
	if PRINT:
		print "Computing scores for %d predictions" % len(pred)		
	from sklearn.metrics import roc_curve, auc, average_precision_score, precision_recall_curve
	
	if CURVES or SAVEFIG != None:
		import matplotlib.pyplot as plt		
		precision, recall, thresholds = precision_recall_curve(real, pred)
		#plt.plot(recall, precision)
		#plt.show()
		fpr, tpr, _ = roc_curve(real, pred)		
		fig, (ax1, ax2) = plt.subplots(figsize=[10.0, 5], ncols=2)
		ax1.set_ylabel("Precision")
		ax1.set_xlabel("Recall")
		ax1.set_title("PR curve")
		ax1.set_xlim(0,1)
		ax1.set_ylim(0,1)
		ax1.plot(recall, precision)
		ax1.grid()
		ax2.plot(fpr, tpr)
		ax2.set_ylim(0,1)
		ax2.set_xlim(0,1)
		ax2.plot([0,1],[0,1],"--",c="grey",)
		ax2.set_xlabel("FPR")
		ax2.set_ylabel("TPR")
		ax2.set_title("ROC curve")
		ax2.grid()
		if SAVEFIG != None:
			plt.savefig(SAVEFIG, dpi=400)
		plt.show()
		plt.clf()
		
	fpr, tpr, thresholds = roc_curve(real, pred)
	auprc = average_precision_score(real, pred)
	aucScore = auc(fpr, tpr)		
	i = 0
	r = []
	while i < len(fpr):
		r.append((fpr[i], tpr[i], thresholds[i]))
		i+=1	
	ts = sorted(r, key=lambda x:(1.0-x[0]+x[1]), reverse=True)[:3]	
	#if PRINT:
	#	print ts
	if threshold == None:
		if PRINT:
			print " > Best threshold: " + str(ts[0][2])
		threshold = ts[0][2]
	i = 0
	confusionMatrix = {}
	confusionMatrix["TP"] = confusionMatrix.get("TP", 0)
	confusionMatrix["FP"] = confusionMatrix.get("FP", 0)
	confusionMatrix["FN"] = confusionMatrix.get("FN", 0)
	confusionMatrix["TN"] = confusionMatrix.get("TN", 0)
	if invert == True:
		while i < len(real):
			if float(pred[i])>=threshold and (real[i]==0):
				confusionMatrix["TN"] = confusionMatrix.get("TN", 0) + 1
			if float(pred[i])>=threshold and real[i]==1:
				confusionMatrix["FN"] = confusionMatrix.get("FN", 0) + 1
			if float(pred[i])<=threshold and real[i]==1:
				confusionMatrix["TP"] = confusionMatrix.get("TP", 0) + 1
			if float(pred[i])<=threshold and real[i]==0:
				confusionMatrix["FP"] = confusionMatrix.get("FP", 0) + 1
			i += 1
	else:
		while i < len(real):
			if float(pred[i])<=threshold and (real[i]==0):
				confusionMatrix["TN"] = confusionMatrix.get("TN", 0) + 1
			if float(pred[i])<=threshold and real[i]==1:
				confusionMatrix["FN"] = confusionMatrix.get("FN", 0) + 1
			if float(pred[i])>=threshold and real[i]==1:
				confusionMatrix["TP"] = confusionMatrix.get("TP", 0) + 1
			if float(pred[i])>=threshold and real[i]==0:
				confusionMatrix["FP"] = confusionMatrix.get("FP", 0) + 1
			i += 1
	#print "--------------------------------------------"
	#print confusionMatrix["TN"],confusionMatrix["FN"],confusionMatrix["TP"],confusionMatrix["FP"]
	if PRINT:
		print "      | DEL         | NEUT             |"
		print "DEL   | TP: %d   | FP: %d  |" % (confusionMatrix["TP"], confusionMatrix["FP"] )
		print "NEUT  | FN: %d   | TN: %d  |" % (confusionMatrix["FN"], confusionMatrix["TN"])	
	
	sen = (confusionMatrix["TP"]/max(0.00001,float((confusionMatrix["TP"] + confusionMatrix["FN"]))))
	spe = (confusionMatrix["TN"]/max(0.00001,float((confusionMatrix["TN"] + confusionMatrix["FP"]))))
	acc =  (confusionMatrix["TP"] + confusionMatrix["TN"])/max(0.00001,float((sum(confusionMatrix.values()))))
	bac = (0.5*((confusionMatrix["TP"]/max(0.00001,float((confusionMatrix["TP"] + confusionMatrix["FN"])))+(confusionMatrix["TN"]/max(0.00001,float((confusionMatrix["TN"] + confusionMatrix["FP"])))))))
	inf =((confusionMatrix["TP"]/max(0.00001,float((confusionMatrix["TP"] + confusionMatrix["FN"])))+(confusionMatrix["TN"]/max(0.00001,float((confusionMatrix["TN"] + confusionMatrix["FN"])))-1.0)))
	pre =(confusionMatrix["TP"]/max(0.00001,float((confusionMatrix["TP"] + confusionMatrix["FP"]))))
	mcc =	( ((confusionMatrix["TP"] * confusionMatrix["TN"])-(confusionMatrix["FN"] * confusionMatrix["FP"])) / max(0.00001,math.sqrt((confusionMatrix["TP"]+confusionMatrix["FP"])*(confusionMatrix["TP"]+confusionMatrix["FN"])*(confusionMatrix["TN"]+confusionMatrix["FP"])*(confusionMatrix["TN"]+confusionMatrix["FN"]))) )  
	
	if PRINT:
		print "\nSen = %3.3f" % sen
		print "Spe = %3.3f" %  spe
		print "Acc = %3.3f " % acc
		print "Bac = %3.3f" %  bac
		#print "Inf = %3.3f" % inf
		print "Pre = %3.3f" %  pre
		print "MCC = %3.3f" % mcc
		print "#AUC = %3.3f" % aucScore
		print "#AUPRC= %3.3f" % auprc
		print "--------------------------------------------"	
	
	return sen, spe, acc, bac, pre, mcc, aucScore, auprc

if __name__ == '__main__':
	main()
