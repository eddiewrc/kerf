# README #

This repo contains a very simple scikit-learn-based implementation of the Random Forest Kernel described in Davies and Ghahramani, 2014. (https://arxiv.org/pdf/1402.4293.pdf) 
THe main difference is that the authors suggest a stochastic quantification of the tree-induced differences (by selecting a node level from an uniform distribution) while here I query the entire path from the root to the leaves.

### What is this repository for? ###

* Didactic purposes and for future development

### How do I get set up? ###

* Python 2.7 should be installed
* matplotlib, scipy, numpy, scikit-learn should be installed (they can be easily installed with pip)

### Who do I talk to? ###

* My email is in the source file